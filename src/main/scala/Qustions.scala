/**
  * Created by mark on 13/03/2017.
  */
object Questions {


  /** 15%
    * 請寫出一個次方函數,pow(x,y)
    * ex:
    * pow(2,3)=2*2*2
    * pow(3,2)=3*3
  **/
  def pow(x:Double,y:Int):Double= {
    if (y==0) 1
    else if (y<0) pow(1/x,-y)
    else x*pow(x,y-1)
  }

  /** 15%
    * 請寫出費利曼數列1,1,2,3,5,8,....
    * ex:
    * fib(1)=1
    * fib(2)=1
    * fib(3)=2
    * fib(n)=fib(n)+fib(n-1)
    **/
  def fib(n:Int):Int= {
    if (n==1) 1
    else if(n==2) 1
    else fib(n-2)+fib(n-1)
  }
  /** 15%
    * 請用Tail Recursive寫出階乘函數fac(n)=n*(n-1)*...*2*1
    * ex:
    * fib(5)=120
    **/
  def tailFac(n:Int,acc:Int):Int= {
    if (n==1) acc
    else tailFac(n-1,n*acc)
  }
  /** 15%
    * 請用Tail Recursive寫出平方階乘函數squareOfFac(n)=[n*n]*[(n-1)*(n-1)]*...*(2*2)*(1*1)
    * ex:
    * squareOfFac(3)=36
    * squareOfFac(5)=14400
    **/
  def tailSquareOfFac(n:Int,acc:Int):Int= {
    if (n==1) acc
    else  tailSquareOfFac(n-1,acc*n*n)
  }

  /** 20%
    * 請寫出一個判斷某數n是否為2的次方數, isPowOf2(n)
    * ex:
    * isPowOf2(2)=true
    * isPowOf2(8)=true
    * isPowOf2(14)=false
    * isPowOf2(32)=true
    * isPowOf2(36)=false
    **/
  def isPowOf2(n:Int):Boolean= {
    if (n==1) true
    else if(n % 2 == 1) false
    else if (n/2 == 1) true
    else isPowOf2(n / 2)
  }


  /** 20%
    * 請用以下函數實現數列加總squareOfSum(n)=1-4+9-16+.....
    * ex:
    * squareOfSum(2)= -3
    * squareOfSum(4)=-10
    * squareOfSum(10)=-55
    * squareOfSum(11)= 66
    **/
  def squareOfSum(n:Int):Int= {
    if(n==1) 1
    else if(n%2==1) n*n+squareOfSum(n-1)
    else -n*n+squareOfSum(n-1)
  }

}
